	/* Hackers' constraints:

a. Ms. Roberts will learn Ruby.
b. Neither Mr. Ford, nor Julia, nor Jack Nicholson will learn Python.
c. Harrison, who is not a Craig, will learn Javascript.
d. Daniel will not learn Perl.

*/

solve(Ans):- 
   Ans = [triple(_,_,javascript), 
	  triple(_,_,perl), 
	  triple(_,_,python), 
	  triple(_,_,ruby)], 
	member(triple(_,roberts,ruby), Ans), %A) Ms Roberts will learn Ruby
	%member(triple(_,roberts,ruby)), R=ruby, %A) Ms Roberts will learn Ruby
		
	%member(triple(_,ford,X), X\==python), %B)people who will not learn python synatx correct?
	member(triple(_,ford,X), Ans),
	%member(triple(_,ford,X)),
	 X\==python, %B)people who will not learn python synatx correct?
    	%member(triple(julia,_,Y), Y\==python),
	member(triple(julia,_,Y), Ans),
	%member(triple(julia,_,Y)), 
	Y\==python,
	%member(triple(jack,nickolson,Z), Z\==python),
	member(triple(jack,nickolson,Z), Ans),
	%member(triple(jack,nickolson,Z)),
	 Z\==python,

	member(triple(harrison,_,javascript), Ans),%C) Harrison, who is not a Craig, will learn Javascript.
	%member(triple(harrison,_,javascript)), J=javascript,
	member(triple(_,craig,P), Ans),
	% member(triple(_,craig,P)), 
	P\==javascript,
	%member(triple(_,craig,P)), P\==javascript,

	%member(triple(daniel,_,T)), 
	%T\==perl,%D)Daniel will not learn Perl.
	member(triple(daniel,_,T), Ans),%D)Daniel will not learn Perl.
	T\==perl.
	%member(triple(daniel,_,T)), T\==perl.%D)Daniel will not learn Perl.  

   % Similarly, the other constraints become 1 or more goals 
   % involving member. Prolog's inequality predicate is \== */

	%  ... remaining constraints written here ...
