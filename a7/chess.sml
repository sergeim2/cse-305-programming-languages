(*
Ariel Aybar UB#50014468
Sergei Mellow UB#50030322
*)
datatype piece = king | queen of int | bishop of int | knight of int | rook of int | pawn of int;

(*Part 1A*)
fun value piece =
	case piece of
	king => 100
	| queen n => 50*n
	| bishop n =>  25*n
	| knight n =>  15*n
	| pawn n => 3*n
	| rook n =>  25*n;

(*Part 1B)*)
fun strength([]) = 0
	|strength(h::t) = value(h) + strength(t);

