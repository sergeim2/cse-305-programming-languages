(*
Ariel Aybar UB#50014468
Sergei Mellow UB#50030322
*)
Control.Print.printDepth := 1337;
datatype 'a ntree = leaf of 'a | node of 'a ntree list;
fun map(f,[]) = 
	[]
	|map(f, x::t) = f(x) :: map(f,t);

fun reduce(f,b,[])= 
	b
	|reduce(f,b,x::t) = f(x,reduce(f,b,t));

(*Part 2B*)
fun toString(leaf(x)) = 
	x
	| toString(node(l)) = let fun h(q,r) = toString(q)^" "^r
	in reduce(h,"",l)
	end;

(*Part 2A*)
fun subst(leaf(x),y,z) = 
	if x = y then leaf(z) else leaf(x)
	| subst(node(l),y,z) = let fun h(a) = subst(a,y,z)
	in node(map(h,l))
	end;
