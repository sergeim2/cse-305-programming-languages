datatype piece = king | queen of int | bishop of int | knight of int | rook of int | pawn of int;

fun value piece =
     case piece of 
      king => 100
    | queen n => 50*n
    | bishop n =>  25*n
    | knight n =>  15*n
    | pawn n => 3*n
    | rook n =>  25*n;
   

fun strength([]) = 0
       |strength(h::t) = value(h) + strength(t);


datatype 'a ntree = leaf of 'a | node of 'a ntree list;

fun reduce(f, b, [])  = b
  | reduce(f, b, h::t) = f(h, reduce(f, b, t));


fun map (f,[]) = []
 |  map(f, h::t) = f(h) :: map(f,t);

fun toString(leaf(x)) = x
        | toString(node(l)) = let fun h(a,b) = toString(a)^" "^b
        in reduce(h,"",l)
        end;

(*fun printTree(leaf(x)) = print(x)
| printTree(node(l)) = let d1 = map(printTree, l)
in ()
end;*)
fun subst(leaf(x),p,t)=
fun substt(leaf(x)) = x
         | substt(node(l)) = let fun h(a,b) = substt(a)^" "^b
         in reduce(h,"",l)
         end;


(*fun toString(leaf(x)) = x
        | toString(node(l)) = let fun h(a,b) = toString(a)^" "^b
        in reduce(h,"",l)
        end;
*)
fun check(a,b,c) string = 
	if a=b then c
	else a;

(*fun toString(leaf(b)) = 0
		 | toString(node([t1, t2]) = 
		 	let val d1 = toString(t1)
	 			val d2 = toString(t2)
 			in if d1 > d2 then 1 + d1
 				else 1 + d2
			end;                 *)
(*datatype 'a Tree = Empty | LEAF of 'a | NODE of ('a Tree) list; 
fun treeToString f NODE = let
    fun treeFun (Empty) = ["(:"]
    | treeFun (NODE(items)) = ["("] @ List.concat (map treeFun items) @ [")"]
    | treeFun (LEAF(v)) = [f v]
    | treeFun (NODE(h::t)) = [""] @ ( treeFun (h)) @ ( treeFun (NODE(t)) )
    in
    String.concat(treeFun Node)
end;*)

